/*
UFCG - Universidade Federal de Campina Grande
LInCE - Laboratório de Inteligência Computacional em Bioenergia
Projeto GASIS3 - GASes Intelligent Sensing - Fase 3
*/

#include <modbus/modbus.h>
#include <stdlib.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <cerrno>
#include <string>
#include <time.h>
#include <sstream>
#include <stdio.h>
#include <libconfig.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <iomanip>
#include <cstdlib>
#include <vector>


using namespace std;

/*
	Crontab: crontab -e
<<<<<<< HEAD:GASBUS_files/gasbus.cpp
     * /2 * * * *  cd /home/lince/GASBUS && ./gasbus

 * 
    To compile: g++ gasbus.cpp -o gasbus -lmodbus -lconfig
    To execute with gasbus.cfg: ./gasbus
    To execute with another config file: ./gasbus -f "durag.cfg"
*/


// Write to file function
void writeToFile( const char * filename, const char * mode, const char * string )
{
   FILE *fp;

   fp = fopen(filename, mode);
   fprintf(fp, "%s", string);
   fclose(fp);

}

// Function used to save date and time from the computer
string getDateTime()
{
    time_t timer;
    char buffer[40];
    struct tm* tm_info;

    time(&timer);
    tm_info = localtime(&timer);
    // year, month, day, hour, minute, second
    strftime(buffer, 25, "%Y_%m_%d_%H_%M_%S", tm_info);
    string dateTime(buffer);
    return dateTime;
}

string getDate()
{
    time_t timer;
    char buffer[40];
    struct tm* tm_info;

    time(&timer);
    tm_info = localtime(&timer);
    // year, month, day, 
    strftime(buffer, 25, "%Y_%m_%d", tm_info);
    string date(buffer);
    return date;
}

int main(int argc, char* argv[])
{
    const char *device = "default"; //tty
    int baud = 0; // baud rate
    const char *parity = "default"; // parity type
    int dataBit = 0;
    int stopBit = 0; 
    const char *sensorName = "default"; //sensor name (contemp, durag or oms)
    int slaveAddresses[10] = {}; //modbus address of the slave device
	int slaveAddress = 0;
	//int start_regAddr = 0; //first register
    //int stop_regAddr = 0; //last register
    const char *outputFile = "default"; //name of the output file
    uint16_t tab_reg[128] = {}; //table of registers
	uint16_t tab_reg2[128]= {}; //table of registers
	uint8_t tab_reg_durag[40]= {}; //table of registers
    int rc = 0; //register counter
	int rc2 = 0; //register counter
	int rc3 = 0;
    int i = 0; //used in for loop
    const char *ovalue = NULL; //value of -o option
	int index = 0;
	int c = 0;
	int f_flag = 0;
	opterr = 0;
	char *fvalue = NULL; //value of -f option
	char etcfvalue[40] = "";
	char cfgfvalue[40] = "";

	//Code to get the options' data 
	while ((c = getopt (argc, argv, "f:")) != -1)
		switch (c)
  		{
		  case 'f':
		    f_flag = 1; 
		    fvalue = optarg; //value from option -f
		    break;
		  default:
		    abort ();
		  }

	// Path of /etc/config.cfg
	const char* homeDir = getenv("HOME");	
	if(f_flag){
		strcpy(etcfvalue, "/etc/");		
		strcat(etcfvalue, fvalue);

		strcpy(cfgfvalue,"/.config/");
		strcat(cfgfvalue,fvalue);
		
		strcat((char*)homeDir,cfgfvalue);	
		}	
	
	else{
		strcpy(etcfvalue,"/etc/gasbus.cfg");
		strcpy(cfgfvalue, "/.config/gasbus.cfg");
		
		strcat((char*)homeDir,cfgfvalue);
	    	}

	config_t cfg;
  	config_init(&cfg);

    	// Read the configuration file. If there is an error, report it and exit. 
	if(! config_read_file(&cfg, homeDir) && ! config_read_file(&cfg, etcfvalue))
	{
		printf("Configuration found not found \n");
		fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
             config_error_line(&cfg), config_error_text(&cfg));
     		config_destroy(&cfg);
     		return(EXIT_FAILURE);
	}

	// Get the sensor name. 
	if(config_lookup_string(&cfg, "sensorname", &sensorName))
		printf("Sensor name: %s\n\n", sensorName);
	else
		fprintf(stderr, "No 'sensorname' setting in configuration file.\n");

	// Getting the data from the configuration file
	config_lookup_string(&cfg, "tty", &device);
	config_lookup_int(&cfg, "baud", &baud);
	config_lookup_string(&cfg, "parity", &parity);
	config_lookup_int(&cfg, "stopbit", &stopBit);
	config_lookup_int(&cfg, "databit", &dataBit);
	//config_lookup_int(&cfg, "start_regAddr", &start_regAddr);
	//config_lookup_int(&cfg, "stop_regAddr", &stop_regAddr);
	config_lookup_string(&cfg, "outputFile", &outputFile);
	config_lookup_string(&cfg, "ovalue", &ovalue);

	//getting the start register
	int start_regAddresses[2]; 
	config_setting_t* start_regAddresses_settings = config_lookup(&cfg, "start_regAddresses");
	int start_regAddresses_size = config_setting_length(start_regAddresses_settings);
	for (int n = 0; n < start_regAddresses_size; ++n)
      {
          start_regAddresses[n] = config_setting_get_int_elem(start_regAddresses_settings, n);
      }
	
	//getting the number of registers
	int number_registers[2]; 
	config_setting_t* number_registers_settings = config_lookup(&cfg, "number_registers");
	int number_registers_size = config_setting_length(number_registers_settings);
	for (int n = 0; n < number_registers_size; ++n)
      {
          number_registers[n] = config_setting_get_int_elem(number_registers_settings, n);
      }
	
	//getting the adresses;
    int addresses[10]; 
	config_setting_t* addresses_settings = config_lookup(&cfg, "slaveAddresses");
	int addresses_size = config_setting_length(addresses_settings);
	for (int n = 0; n < addresses_size; ++n)
      {
          addresses[n] = config_setting_get_int_elem(addresses_settings, n);
      }

	int k = 0;
	while(k<addresses_size){
	slaveAddress = addresses[k];

	// Printing the data
	cout << endl;
    printf("%s, %d, %c, %d, %d, %s, %d, %s\n", device, baud, parity[0], dataBit, stopBit, sensorName, slaveAddress, outputFile);
    cout << getDateTime() << endl;
	
    modbus_t* ctx;

	// Configuration of modbus
    ctx = modbus_new_rtu(device, baud, parity[0], dataBit, stopBit);
    if (ctx == NULL)
    {
        std::cout << "Unable to create the libmodbus context." << std::endl;
    }
    modbus_set_debug(ctx, 1);
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec =  500000;
    modbus_set_slave(ctx, slaveAddress);
    printf("errno: %s\n", modbus_strerror(errno));
    modbus_set_response_timeout(ctx, &timeout);


    if ( modbus_connect(ctx) == -1)
    {
        std::cout << "Connection failed." << std::endl;
    }

    std::cout << "connection success" << std::endl;

    struct timeval timeout_begin_old;
    struct timeval timeout_end_old;

    // Save original timeout 
    modbus_get_response_timeout(ctx, &timeout_begin_old);
    printf(" ---> timeout begin tv_sec %ld and timeout tv_usec %ld\n", timeout_begin_old.tv_sec, timeout_begin_old.tv_usec);

    modbus_get_byte_timeout(ctx, &timeout_end_old);
    printf(" ---> timeout end tv_sec %ld and timeout tv_usec %ld\n", timeout_end_old.tv_sec, timeout_end_old.tv_usec);
	
	//Setting slave address
    modbus_set_slave(ctx, slaveAddress);
    modbus_set_debug(ctx, TRUE);

	//Setting the conection
    if (modbus_connect(ctx) == -1) {
        fprintf(stderr, "Connexion failed: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
		k++;
		continue;
    }

	//Reading the registers
	int rc = 0;
    rc = modbus_read_registers(ctx, start_regAddresses[0], number_registers[0], tab_reg);

	//Repeating measurement after timeout or CRC error (3 attempts)
	int cont = 0;
	while (rc==-1 && cont <= 3){
			cont = cont + 1;
			cout << "Connection error, repeating measurement" << endl << endl;
			rc = modbus_read_registers(ctx, start_regAddresses[0], number_registers[0], tab_reg);
			}

	if (rc == -1) {
        fprintf(stderr, "%s\n", modbus_strerror(errno));
		k++;
		continue;
    }	

		
	if(start_regAddresses_size>1){
		int rc2 = 0;
		rc2 = modbus_read_registers(ctx, start_regAddresses[1], number_registers[1], tab_reg2);
		cont = 0;
		//Repeating measurement after timeout or CRC error (3 attempts)
		while (rc2==-1 && cont <= 3){
			cont = cont + 1;
			cout << "Connection error, repeating measurement" << endl << endl;
			rc2 = modbus_read_registers(ctx, start_regAddresses[1], number_registers[1], tab_reg2);		
		}

		if (rc2 == -1) {
        fprintf(stderr, "%s\n", modbus_strerror(errno));
		k++;
		continue;
    }	 
	}

	//Printing the registers
    for (i=0; i < rc; i++) {
		if (!strcmp(ovalue,"%d"))
    		printf("reg[%d]=%d \n", i+start_regAddresses[0], tab_reg[i]);
		else if (!strcmp(ovalue,"%X"))
			printf("reg[%d]=%X \n", i+start_regAddresses[0], tab_reg[i]);
		else
			printf("Invalid format (-o) - choose between %%d, %%X");
    }

	if(start_regAddresses_size>1){
		for (i=0; i < rc2; i++) {
			if (!strcmp(ovalue,"%d"))
				printf("reg[%d]=%d \n", i+start_regAddresses[1], tab_reg2[i]);
			else if (!strcmp(ovalue,"%X"))
				printf("reg[%d]=%X \n", i+start_regAddresses[1], tab_reg2[i]);
			else
				printf("Invalid format (-o) - choose between %%d, %%X");
		}
	}
	//Data to file
    std::stringstream ss;
    string dateTime = getDateTime();
	string date = getDate();

	//Contemp registers
	if(!strcmp(sensorName,"contemp")){
		ss << fixed << showpoint << setprecision (2);
		ss << dateTime << " ; ";
		ss << slaveAddress << " ; ";
		float reg0 = (float)tab_reg[0]/100;
		float reg1 = (float)tab_reg[1]/100;
		ss << reg0 << ";";
		ss << reg1 << endl;
		}

	//DURAG registers
	else if(!strcmp(sensorName,"durag")){
	//Reconstruction

		uint32_t reg2_3 = (uint32_t)tab_reg[2] << 16 | (uint32_t)tab_reg[3];
		float diffPress = *((float*)&reg2_3);
		uint32_t reg6_7 = (uint32_t)tab_reg[6] << 16 | (uint32_t)tab_reg[7];
		float temperatureKelvin = *((float*)&reg6_7);
		uint32_t reg16_17 = (uint32_t)tab_reg[16] << 16 | (uint32_t)tab_reg[17];
		float concentration = *((float*)&reg16_17);
		uint32_t reg18_19 = (uint32_t)tab_reg[18] << 16 | (uint32_t)tab_reg[19];
		float velocity = *((float*)&reg18_19);		
		uint32_t reg20_21 = (uint32_t)tab_reg[20] << 16 | (uint32_t)tab_reg[21];
		float absPressure = *((float*)&reg20_21);		
		uint32_t reg22_23 = (uint32_t)tab_reg[22] << 16 | (uint32_t)tab_reg[23];
		float temperatureCelsius = *((float*)&reg22_23);		
		uint32_t reg24_25 = (uint32_t)tab_reg[24] << 16 | (uint32_t)tab_reg[25];
		float volumetricFlow = *((float*)&reg24_25);		
		uint32_t reg26_27 = (uint32_t)tab_reg[26] << 16 | (uint32_t)tab_reg[27];
		float concentrationStd = *((float*)&reg26_27);
		uint32_t reg28_29 = (uint32_t)tab_reg[28] << 16 | (uint32_t)tab_reg[29];
		float volumetricFlowStd = *((float*)&reg28_29);		
		uint32_t reg30_31 = (uint32_t)tab_reg[30] << 16 | (uint32_t)tab_reg[31];
		float massFlow = *((float*)&reg30_31);

		ss << fixed << showpoint << setprecision (2);
		ss << dateTime << " ; ";
		ss << slaveAddress << " ; ";
		ss << temperatureKelvin << " ; "; // [K]
		ss << concentration << " ; "; // [mg/m³]
		ss << velocity << " ; "; // [m/s]
		ss << absPressure << " ; "; // [hPa]
		ss << temperatureCelsius << " ; "; // [ºC]
		ss << volumetricFlow << " ; "; // [m³/h]
		ss << concentrationStd << " ; "; // [mg/Nm³]
		ss << volumetricFlowStd << " ; "; // [Nm³/h]
		ss << massFlow << " ; "; // [kg/h]

		
		// DURAG errors
		int rc3 = 0;
		int cont = 0;
		rc3 = modbus_read_input_bits(ctx, 0, 45, tab_reg_durag);
		cout << "rc3: " << rc3 << endl;
		while (rc3==-1){
			cont = cont + 1;
			cout << "Connection error, repeating measurement" << endl;
			rc3 = modbus_read_input_bits(ctx, 0, 45, tab_reg_durag);
			cout << "rc3: " << rc3 << endl;

			if (cont>=3)
				break;
			
		}   
		cout << "Error readings:" << endl;
		for (i=0; i < rc3; i++) {
    		printf("reg[%d]=%d \n", i, tab_reg_durag[i]);
		}
		
		ss << int(tab_reg_durag[0]) << " ; ";
		ss << int(tab_reg_durag[1]) << " ; ";
		ss << int(tab_reg_durag[16]) << " ; ";
		ss << int(tab_reg_durag[21]) << " ; ";
		ss << int(tab_reg_durag[30]) << " ; ";
		ss << int(tab_reg_durag[31]) << " ; ";
		ss << int(tab_reg_durag[32]) << " ; ";
		ss << int(tab_reg_durag[33]) << " ; ";
		ss << int(tab_reg_durag[34]) << " ; ";
		ss << int(tab_reg_durag[35]) << " ; ";
		ss << int(tab_reg_durag[36]) << " ; ";
		ss << int(tab_reg_durag[37]) << " ; ";
		ss << int(tab_reg_durag[38]) << " ; ";
		ss << int(tab_reg_durag[39]) << " ; ";
		ss << int(tab_reg_durag[40]) << endl;
	}

	//OMS registers
	else if(!strcmp(sensorName,"oms")){
		ss << fixed << showpoint << setprecision (2);
		ss << dateTime << " ; ";
		ss << slaveAddress << " ; ";
		float o2 = (float)tab_reg[3]/100;
		ss << o2 << " ; ";
		uint32_t errorflag = tab_reg[0];
		uint32_t status = tab_reg[2];
		ss << errorflag << " ; ";
		ss << status << endl;
	}
	
	//SWG registers
	else if(!strcmp(sensorName,"swg")){
	//Reconstruction
		uint32_t reg20_21 = (uint32_t)tab_reg[20] << 16 | (uint32_t)tab_reg[21];
		float O2 = *((float*)&reg20_21);
		uint32_t reg22_23 = (uint32_t)tab_reg[22] << 16 | (uint32_t)tab_reg[23];
		float CO2 = *((float*)&reg22_23);
		uint32_t reg24_25 = (uint32_t)tab_reg[24] << 16 | (uint32_t)tab_reg[25];
		float CO = *((float*)&reg24_25);			
		uint32_t reg26_27 = (uint32_t)tab_reg[26] << 16 | (uint32_t)tab_reg[27];
		float NO = *((float*)&reg26_27);		
		uint32_t reg32_33 = (uint32_t)tab_reg[32] << 16 | (uint32_t)tab_reg[33];
		float SO2 = *((float*)&reg32_33);
		uint32_t reg38_39 = (uint32_t)tab_reg[38] << 16 | (uint32_t)tab_reg[39];
		float CH4 = *((float*)&reg38_39);


		uint32_t reg92_93 = (uint32_t)tab_reg[92] << 16 | (uint32_t)tab_reg[93];
		float CO_mg = *((float*)&reg92_93);			
		uint32_t reg94_95 = (uint32_t)tab_reg[94] << 16 | (uint32_t)tab_reg[95];
		float NO_mg = *((float*)&reg94_95);		
		uint32_t reg100_101 = (uint32_t)tab_reg[100] << 16 | (uint32_t)tab_reg[101];
		float SO2_mg = *((float*)&reg100_101);
		uint32_t reg106_107 = (uint32_t)tab_reg[106] << 16 | (uint32_t)tab_reg[107];
		float CH4_mg = *((float*)&reg106_107);

		uint32_t reg40_41 = (uint32_t)tab_reg[40] << 16 | (uint32_t)tab_reg[41];
		float C3H8 = *((float*)&reg40_41);
		uint32_t reg42_43 = (uint32_t)tab_reg[42] << 16 | (uint32_t)tab_reg[43];
		float C6H14 = *((float*)&reg42_43);
		uint32_t reg54_55 = (uint32_t)tab_reg[54] << 16 | (uint32_t)tab_reg[55];
		float TGas = *((float*)&reg54_55);
		uint32_t reg56_57 = (uint32_t)tab_reg[56] << 16 | (uint32_t)tab_reg[57];
		float TAmb = *((float*)&reg56_57);
		uint32_t reg60_61 = (uint32_t)tab_reg[60] << 16 | (uint32_t)tab_reg[61];
		float DPress = *((float*)&reg60_61);
		uint32_t reg62_63 = (uint32_t)tab_reg[62] << 16 | (uint32_t)tab_reg[63];
		float FSpeed = *((float*)&reg62_63);
		uint32_t reg66_67 = (uint32_t)tab_reg[66] << 16 | (uint32_t)tab_reg[67];
		float Revol = *((float*)&reg66_67);
		uint32_t reg74_75 = (uint32_t)tab_reg[74] << 16 | (uint32_t)tab_reg[75];
		float FVolume = *((float*)&reg74_75);
		uint32_t reg76_77 = (uint32_t)tab_reg[76] << 16 | (uint32_t)tab_reg[77];
		float TSensor = *((float*)&reg76_77);
		uint32_t reg78_79 = (uint32_t)tab_reg[78] << 16 | (uint32_t)tab_reg[79];
		float SGasFlow = *((float*)&reg78_79);
		uint32_t reg84_85 = (uint32_t)tab_reg[84] << 16 | (uint32_t)tab_reg[85];
		float THose = *((float*)&reg84_85);
		uint32_t reg306_307 = (uint32_t)tab_reg2[6] << 16 | (uint32_t)tab_reg2[7];
		float ExcAir = *((float*)&reg306_307);
		uint32_t reg310_311 = (uint32_t)tab_reg2[10] << 16 | (uint32_t)tab_reg2[11];
		float Losses = *((float*)&reg310_311);
		uint32_t reg312_313 = (uint32_t)tab_reg2[12] << 16 | (uint32_t)tab_reg2[13];
		float Effic = *((float*)&reg312_313);
		uint32_t reg314_315 = (uint32_t)tab_reg2[14] << 16 | (uint32_t)tab_reg2[15];
		float LossesCondens = *((float*)&reg314_315);
		uint32_t reg316_317 = (uint32_t)tab_reg2[16] << 16 | (uint32_t)tab_reg2[17];
		float EfficCondens = *((float*)&reg316_317);


		uint32_t reg18_19 = tab_reg[18] << 16 | tab_reg[19];
		uint32_t Status = reg18_19;
		uint32_t site = (Status >> 29);
		uint16_t O2ref = tab_reg[13];
		cout << "TGas : " << TGas << endl;
		cout << "SGASFLOW: " << SGasFlow << endl;
		uint32_t StatusBits = (Status << 12);
		StatusBits = (StatusBits >> 28);
		
		uint32_t SensorZeroError = (Status << 11);
		SensorZeroError = (SensorZeroError >> 31);
		
		uint32_t MajorAnalyserError = (Status << 10);
		MajorAnalyserError = (MajorAnalyserError >> 31);
			
		uint32_t MinorError = (Status << 9);
		MinorError = (MinorError >> 31);
		
		cout << "Status: " << hex << Status << endl;

		//ss << fixed << showpoint << setprecision (2);
		ss << dateTime << " ; ";
		ss << site << " ; "; 
		ss << O2 << " ; ";
		ss << fixed << showpoint << setprecision(2) << CO << " ; ";
		ss << CO2 << " ; ";
		ss << NO << " ; ";
		ss << SO2 << " ; ";
		ss << CH4 << " ; ";
		ss << CO_mg << " ; ";		
		ss << SGasFlow << " ; ";
		ss << StatusBits << " ; ";
		ss << SensorZeroError << " ; ";
		ss << MajorAnalyserError << " ; ";
		ss << MinorError << endl;


		/*ss << TSensor << " ; ";
		ss << THose << " ; ";
		ss << NOMg << endl;
		
	
		ss << C3H8 << " ; ";
		ss << C6H14 << " ; ";
		ss << TGas << " ; ";
		ss << TAmb << " ; ";
		ss << DPress << " ; ";
		ss << FSpeed << " ; ";
		ss << Revol << " ; ";
		ss << FVolume << " ; ";
		ss << TSensor << " ; ";
		ss << SGasFlow << " ; ";
		ss << ExcAir << " ; ";
		ss << Losses << " ; ";
		ss << Effic << " ; ";
		ss << LossesCondens << " ; ";
		ss << EfficCondens << endl;*/
		
		slaveAddress = site;
	}

    	//convert the stream buffer into a string
    	std::string message = ss.str();

	//Write to the chosen output file

	stringstream temp_Slavestr;
	temp_Slavestr<<(slaveAddress);
	std::string Slavestr = temp_Slavestr.str();
	const char* slaveChar = Slavestr.c_str();

	char *outputFileC[2];
	int length = strlen(outputFile);
	outputFileC[1] = new char[length +100]();
	strncpy(outputFileC[1], outputFile, length+100); 

	//char* outputFileC = (char*) outputFile;
	const char * dateC = date.c_str();
	strcat(outputFileC[1], slaveChar);
	strcat(outputFileC[1], "_");
	strcat(outputFileC[1], dateC);
	strcat(outputFileC[1], ".dat");
    writeToFile(outputFileC[1], "a", message.c_str());
	
	memset(tab_reg, 0, sizeof(tab_reg));
	modbus_close(ctx);
   	modbus_free(ctx);
	k++;
	}
    return 0;

}
